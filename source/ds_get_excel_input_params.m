function [  input_params_struct, error_message  ] = ...
    ds_get_excel_input_params( an_excel_file_name,excel_tab_number)
%ds_get_excel_input_params  returns a cell with the set input params:
% Input:  an_excel_file_name  Excel file name (full path) with data to be processed with the die_scrap app.
%         excel_tab_number    Excel tab number(BE AWARE THAT EVEN HIDEN TABS ARE PART OF EXCEL NUMBERING:
%                             Warning: Do not hide tabs in excel: It is very misleading.
%
% Output: a cell with 6 entries: in two columns:
% Assumptions:
%   1 - column one is the parameter name
%   2 - column 2 is the parameter value
%   3 - column 3 is the description: Ignored.
%   4 - the indices are in ascending order. and there are no dupplicated indices.error_message  
% sample_call:
% [  input_params_struct, error_message  ] = ...
%     ds_get_excel_input_params( 'C:\Users\jsanchez\Documents\die_scrap\configure\die_scap_configure.xlm',1)
error_message           = '';
input_params_struct     = struct;
input_params_cnt        = 8; % jas_jardcoded: number of expected input parameters in the excel file. Beyond that it will be ignored.
% read the whole sheet as a table:
sheet_table= readtable(an_excel_file_name...
    ,'ReadVariableNames',false,'FileType','spreadsheet'...
    ,'Sheet',excel_tab_number  );
cur_row             = 0;
row_found_cnt       = size(sheet_table,1); %
col_found_cnt       = size(sheet_table,2); %
if ( (col_found_cnt <2) ||  (row_found_cnt <input_params_cnt) )
    error_message = 'ERROR: Invalid Excel file format. Reading input parameters. Fix it and try again. \nExpect at least 2 cols and 11 rows!';
    return;
end

try
    input_params_names  = table2cell(sheet_table(2:input_params_cnt+1,1)); % first col is the  ds_names param
    input_params_names  = strrep(input_params_names,'''','');
    input_params_values = table2cell(sheet_table(2:input_params_cnt+1,2)); % second col is the ds_values param
    
%     cfg_params_wanted_cel = { 'webservice_url','wo_number','target_op_code'...
%         ,'warehouse_bin','warehouse_loc','user_name','trans_type','comment' ...
%          };
    
        cfg_params_wanted_struct = struct('webservice_url',''... 
            ,'wo_number',''... 
            ,'target_op_code',''...
            ,'warehouse_bin',''...
            ,'warehouse_loc',''...
            ,'user_name',''...
            ,'trans_type',''...            
            ,'comment',''...          
            );
    
    found_fields = isfield(cfg_params_wanted_struct, input_params_names);
    if (sum(found_fields) == input_params_cnt)
        % FOUND ALL THE NEEDED PARAMETERS: Assume they are in order.
        cur_row = 1;
        input_params_struct.webservice_url   = input_params_values{1};
        cur_row = 2;
        input_params_struct.wo_number     	= input_params_values{2};
        cur_row = 3;
        input_params_struct.target_op_code 	= input_params_values{3};
        cur_row = 4;
        input_params_struct.warehouse_bin 	= input_params_values{4};
        cur_row = 5;
        input_params_struct.warehouse_loc  	= input_params_values{5};        
        cur_row = 6;
        input_params_struct.user_name   	= input_params_values{6};
        cur_row = 7;
        input_params_struct.trans_type   	= input_params_values{7};        
        cur_row = 8;
        input_params_struct.comment     	= input_params_values{8};    
        cur_row = 9;                                                  % 9 is the die_scrap_data
        % OK TAB_1: configure. Now read tab_2: data
        input_params_struct.fail_chip_serials = {};
        [ die_scrap_data, error_message ] = ds_get_excel_range( an_excel_file_name ,2);
        if (~(isempty(error_message)))
            fprintf('\n Error: Die Scraps read count   = %-3d ',size(die_scrap_data,1));
            fprintf('\n Error: Partial: Reading Die Scraps    ... ERROR           ');
            return;
        else
            % OK READING DATA: Save it
            input_params_struct.fail_chip_serials = die_scrap_data;
            fprintf('\n Die Scraps read count   = %-3d ',size(die_scrap_data,1));
            fprintf('\n Reading Die Scraps    ... DONE           ');            
        end
    else
        error_message = 'ERROR: Invalid Excel file format. Fix it and try again:\n';
        fprintf('\n%s',error_message);        
        fprintf('\n webservice_url	= %-s',input_params_values{1});
        fprintf('\n wo_number    	= %-s',input_params_values{2});
        fprintf('\n target_op_code	= %-s',input_params_values{3});
        fprintf('\n warehouse_bin 	= %-s',input_params_values{4});
        fprintf('\n warehouse_loc 	= %-s',input_params_values{5});        
        fprintf('\n user_name    	= %-s',input_params_values{6});
        fprintf('\n trans_type    	= %-s',input_params_values{7});        
        fprintf('\n comment     	= %-s',input_params_values{8});
        return;
    end
catch
    error_message = sprintf('ERROR: Invalid Input Parameters Excel file format. \nFix it and try again. Error Reading row: %4d',cur_row);
    return;
end % catch
fprintf('\n Configuration Parameters:    \n');
fprintf('\n webservice_url  = %-s',input_params_values{1});
fprintf('\n wo_number     	 = %-s',input_params_values{2});
fprintf('\n target_op_code  = %-s',input_params_values{3});
fprintf('\n warehouse_bin 	 = %-s',input_params_values{4});
fprintf('\n warehouse_loc 	 = %-s',input_params_values{5});
fprintf('\n user_name       = %-s',input_params_values{6});
fprintf('\n trans_type      = %-s',input_params_values{7});
fprintf('\n comment         = %-s',input_params_values{8});
end % fn ds_get_excel_input_params
     





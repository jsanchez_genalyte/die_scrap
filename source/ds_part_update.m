function [ error_message ] = ds_part_update( input_params )
%DS_PART_UPDATE  updates the part for the chips defined for a given die scrap list of chips, and a wo).
%   Detailed explanation goes here
% pipleine:  ds_part_update  --> ds_read_sp_master_file -->
%                
error_message = '';
debug_flag = false; % version 
if (debug_flag)
    input_params
end
webservice_url = input_params.webservice_url; % 'http://10.0.2.226/runcard/soap?wsdl';          % for real
%                                             % 'http://10.0.2.226/runcard_test/soap?wsdl';    % test ONLY
wo_number                   = input_params.wo_number;
target_op_code              = input_params.target_op_code; % 'Q410'; ?Optical Test Evaluation? operation code for test step
warehouse_bin               = input_params.warehouse_bin;  % 'SCRAP';              % 'fix' Bin assignment required in RunCard
warehouse_loc               = input_params.warehouse_loc;  % 'Scrap Bin';          % 'fix' Location defined in RunCard to place scrapped chips
user_name                   = input_params.user_name;      % 'jsanchez';           % 'fix''mornelas'
trans_type                  = input_params.trans_type;     % 'Scrap';              % 'fix'
comment                     = input_params.comment;        %  'FSR Failures';       % 'fix'


createClassFromWsdl(webservice_url);
obj = runcard_wsdl;
methods(obj)


%fail_chip_serials = cell(0);

% fail_chip_serials = {...
% 'P153059.24.013.003'; ...
% 'P153059.24.014.003'; ...
% 'P153059.24.015.007'};
 
%check the status of each serial number
for ndx=1:size(input_params.fail_chip_serials,1)
    
    serial = input_params.fail_chip_serials{ndx};   % jas_here_mario_define_sample_input
    
    [response, error, msg] = getUnitStatus(obj,serial);
    if error ~= 0
        disp(['RunCard exception encountered : ' msg    ' No die scrapping done at all' ]);
        return
    end
%     if ~strcmp(response.opcode, target_op_code)
%         disp(['Invalid opcode ' response.opcode ', expecting ' target_op_code ' skipping serial '  serial ]);
%         continue
%     end
    
    if ~strcmp(response.workorder, wo_number)
        error_message = sprintf ('Invalid workorder %-s  expecting %-s skipping serial %-s ', response.workorder ,wo_number, response.serial);
        disp(error_message);        
        continue
    end    
    
    if ~strcmp(response.status, 'IN QUEUE') && ~strcmp(response.status, 'IN PROGRESS')
        error_message = sprintf ('Invalid status %-s  for serial %-s Skipped ', response.status , response.serial  );
        disp(error_message);
        continue
    end
 
    disp([response.workorder ': ' response.serial ' is ' response.status]);

    transactionInfo.username        = user_name;
    transactionInfo.transaction     = trans_type;
    transactionInfo.serial          = response.serial;
    transactionInfo.workorder       = response.workorder;
    transactionInfo.seqnum          = response.seqnum;
    transactionInfo.opcode          = response.opcode;
    
    transactionInfo.warehousebin    = warehouse_bin;
    transactionInfo.warehouseloc    = warehouse_loc;
    transactionInfo.comment         = comment;
    
    transactionData                 = '';
    transactionBOM                  = '';
 
    [error, msg] = transactUnit(obj,transactionInfo, transactionData, transactionBOM);
    if error ~= 0
        disp(['RunCard exception encountered when doing a ' transactionInfo.transaction ' transaction: : ' msg]);
        return
    else
        disp(msg);
    end 
end % for each failure chip serial

end % fn ds_part_update

%             ,'target_op_code',''...
%             ,'warehouse_bin',''...
%             ,'warehouse_loc',''...
%             ,'user_name',''...
%             ,'trans_type',''...            
%             ,'comment',''...
                

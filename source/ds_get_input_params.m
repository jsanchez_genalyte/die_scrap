function [ input_params_struct,error_message ] = ds_get_input_params( )
%DS_GET_INPUT_PARAMS Retrieves input params for the die_scrap app.
%   Returns a struct with the params.
%   input files: hardcoded files: .m or .xlsm sitting in the ..\configure\ directory.

error_message                   = '';
%cfg_params_config_path         = 'W:\die_scrap\';                          % End User to be copied to ..\cfg_and_data\
%cfg_params_config_path          = 'C:\Users\jsanchez\Documents\die_scrap\configure\'; % Jas's  file for dbg

logical_str                     = {'FALSE', 'TRUE'};
die_scrap_root_dir              = 'die_scrap';         % jas_hardcoded_expected  C:\Users\jsanchez\Documents\dcrc\source\cfg\die_scrap_cfg.m
if (isdeployed)
    % COMPILED VERSION USED A .xlsm FILE FOR CONFIGURATION
    cfg_params_file_name        = 'die_scrap_configure.xlsm'; % jas_hardcoded_expected in this dir: ..\cfg\ relative to the exe
else
    % SCRIPT VERSION USED A .m FILE FOR CONFIGURATION
    cfg_params_file_name        = 'die_scrap_configure.xlsm';
    %cfg_params_file_name        = 'die_scrap_configure.m'; % jas_hardcoded_expected in this dir: ..\cfg\ relative to the source
end
cur_pwd                         = pwd;
input_params_struct             = struct;
ndx                             = strfind(cur_pwd,die_scrap_root_dir);
if ( (~(isempty(ndx)))  && (ndx > 1) )
    % FOUND MAIN DIR FOR THE APP: die_scrap
    base_dir                    = cur_pwd(1:ndx+8); %jas_hard_coded to get the base_dir: function of the length of the string: die_scrap
    cfg_dir                    = strcat(base_dir,'\configure\');
    
    % Check if the cfg folder exists:
    if ( ( exist(cfg_dir,'dir') == 7))
        % Folder Exist and it is a directory: We are almost done.
        cfg_params_full_name =strcat(cfg_dir,cfg_params_file_name);
    else
        % Folder does not exist: Prompt for loc of input excel file.
        error_message = sprintf('ERROR: die_scrap can not run due to missing: \nconfigure directory: %s\n',cfg_dir);
        return
    end
    % DS CONFIGURE DIRECTORY EXIST Try to pen cfg file
    
    if ((isdeployed) || ( strcmp(cfg_params_full_name(end-3:end),'xlsm')))
        % COMPILED VERSION USED A .xlsm FILE FOR CONFIGURATION: read it.
        excel_tab_number            = 1;
        [input_params_struct,error_message] =  ds_get_excel_input_params( cfg_params_full_name,excel_tab_number);
        if ((~isempty(error_message)) && (strcmpi(error_message(1:5),'ERROR')))
            return;
        end
        if ( (~isempty(input_params_struct)) &&  ((isempty(error_message)) || (strcmp(error_message(1:7),'WARNING')) ) )
            fprintf('\n Reading CONFIG PARAMETERS ... DONE ');
            return
        else
            fprintf('\n Reading CONFIG PARAMETERS ... ERROR\n');
            fprintf('%s',error_message);
        end
        return;
    end; % deployed
    % SCRIPT VERSION USED A .m FILE FOR CONFIGURATION
else
    error_message = sprintf('ERROR: die_scrap APP can not run due to missing: \nroot directory: %s\nSee read_me.txt file in the doc directory\n',die_scrap_root_dir);
end % dir exist.

end % fn: read_cfg_params_file


% Main file: die_scrap.m
% Script to perform the die scrap given 1 excel file: die_scrap_configure, with 2 tabs: 1_ die_scrap_configure 2_ die_scrap_data
% die_scrap_app:
% %       h = matlab.desktop.editor.getAll; h.close
%     cd 'C:\Users\jsanchez\Documents\die_scrap\source\'
% to compile:
%   mcc -m  die_scrap -a ..\configure  -o  die_scrap
%   mcc -m  die_scrap  -a ..\configure  -o  die_scrap -R 'logfile','die_scrap.log'

%   !copy die_scrap.exe C:\Users\jsanchez\Documents\die_scrap\exe\
%   !move die_scrap.exe \\genstore2\Users\Storage\jsanchez\Documents\die_scrap\exe\
%   ml_doc: https://www.mathworks.com/products/compiler/supported/compiler_support.html

% Define Arguments: (see end of this file)

% to test:  runcard_test: use wo: DWO-0051 

% pipeline: die_scrap --> ds_get_input_params  --> ds_get_excel_input_params (excel)  -->  sp_get_excel_range (excel)
%                     --> ds_part_bin_update  
% Rev_history
% Rev 1.00 Date: 2018_04_17 First compiled version: (clone from the die_scrap app) 
run_date = datestr(now);
fprintf('\n Initializing Die Scrap:   Rev 1.00  Production \n Run Date-time                 %-s\n',run_date);
[ input_params,error_message ] = ds_get_input_params();
if (~(isempty(error_message)))
    fprintf('\n ERROR:\n%s\n',error_message);
    fprintf('\n');
    pause(20);
    return;
end
[ error_message ]  = ds_part_update( input_params);
if ((~(isempty(error_message))) && (~(strcmpi(error_message,'Inventory part bin updated'))))
    fprintf('\nERROR:,\n%s\n',error_message);
end
fprintf('\n\tDIE SCRAP REQUEST DONE_________________________\n\n'); 
% _______________________________________________________________________________________________________
if (isdeployed)
    fprintf('\n To produce a TEXT report of this output:  \n');    
    fprintf('\n    1. Copy to the clipboard all the output do: Edit_menu --> Select All --> Enter  ');
    fprintf('\n    2. Paste into an email message: CTRL-V to share the report with the Die Scrap Requester. \n');      
    fprintf('\n To verify results: Follow these steps in RunCard:  \n');  
    fprintf('\n   1. Search for the needed WO in Terminal');  
    fprintf('\n   2. In the terminal view select  ... more input from Mario ...');  
    fprintf('\n   3. In the View List drop-down select carrier view. ... more input from Mario ...');  
    fprintf('\n   4. The colors of each Gelpack in carrier view should reflect the new part_bin_numbers ... more input from Mario ...');
    fprintf('\n\n Bye ... \n');    
    % ALLOW THE USER TO DO CTRL-A CTRL-C
    pause(40); 
end
